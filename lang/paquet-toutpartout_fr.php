<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'toutpartout_nom' => '{tout} partout',
	'toutpartout_slogan' => 'Permet d’afficher toutes les rubriques ou auteurs, même lorsque ce n’est pas publié',
	'toutpartout_description' => '',
);
